let navItems = document.querySelector("#navSession");
let pathName = window.location.pathname.split('/').pop();
// local storage is a built-in object
// an object used to store information in our clients/devices
let userToken = localStorage.getItem("token");
console.log(userToken);

let path = './';
if(pathName === "index.html" || pathName === ""){
	path = "pages/"
}

if(!userToken) {
	navItems.innerHTML = 
		`
		<li class="nav-item">
			<a href="${path}register.html" class="nav-link">Register</a>
		</li>
		<li class="nav-item">
			<a href="${path}login.html" class="nav-link">Login</a>
		</li>
		`;
}
else {
	navItems.innerHTML = 
	`
	<li class="nav-item">
		<a href="${path}profile.html" class="nav-link"> Profile </a>
	</li>
	<li class="nav-item">
		<a href="${path}logout.html" class="nav-link">Logout</a>
	</li>
	`;
}