// instantiate a URLSearchParams object so we can execute methods to access the parameters
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');

let token = localStorage.getItem('token');

let editCourseForm = document.querySelector("#editCourse");
let courseName = document.querySelector("#courseName");
let coursePrice = document.querySelector("#coursePrice");
let courseDescription = document.querySelector("#courseDescription");

//get the course details
fetch(`https://mighty-lake-39348.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	courseName.value = data.name;
	coursePrice.value = data.price;
	courseDescription.value = data.description;
});

editCourseForm.addEventListener('submit', (e) => {
	e.preventDefault();
	if(courseName.value !== '' && courseDescription.value !== '' && coursePrice.value !== ''){
		const formData = {
			courseId: courseId,
			name: courseName.value,
			price: coursePrice.value,
			description: courseDescription.value
		}
		fetch(`https://mighty-lake-39348.herokuapp.com/api/courses/`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify(formData)
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			alert('Successfully updated course details!');
			window.location.replace('courses.html');
		});
	}
	else{
		alert('All fields are required');
	}
});