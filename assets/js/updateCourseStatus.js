let token = localStorage.getItem('token');
let courseId = new URLSearchParams(window.location.search).get('courseId');
let newStatus = new URLSearchParams(window.location.search).get('status');

fetch(`https://mighty-lake-39348.herokuapp.com/api/courses/${courseId}/status/${newStatus}`, {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json',
		'Authorization' : `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	console.log(data);
	window.location.replace("courses.html");
});