// let navItems = document.querySelector("#navSession");
// retrieve the user information for isAdmin
let adminUser = localStorage.getItem('isAdmin');
console.log(adminUser);
// will contain the html for the different buttons per user
let cardFooter;
let adminViewBtn = '';

// event listener if admin views a specific course
document.addEventListener('click', (e) => {
	if(e.target && e.target.classList.contains('view-course-btn')){
		let courseId = e.target.dataset.courseid;
		let viewCourseTitle = document.querySelector("#viewCourseTitle");
		let viewCourseBody = document.querySelector("#viewCourseBody");
		fetch(`https://mighty-lake-39348.herokuapp.com/api/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			viewCourseTitle.textContent = `${data.name} Enrolled Students`;
			if(data.enrollees.length === 0){
				viewCourseBody.innerHTML = "<h6 class='text-center'>No enrollees</h6>";
			}
			else{
				let enrolleeIds = data.enrollees.map(enrollee => enrollee.userId);
				let enrolleesHTML = "";

				viewCourseBody.innerHTML = '<ul id="enrolleeList" class="list-group list-group-flush"></ul>';

				fetch(`https://mighty-lake-39348.herokuapp.com/api/users/enrollees`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({enrolleeIds: enrolleeIds})
				})
				.then(res => res.json())
				.then(data => {
					let enrolleeList = document.querySelector("#enrolleeList");
					data.forEach((enrollee, index) => {
						enrolleeList.innerHTML += 
							`
								<li class="list-group-item">${index+1}. ) ${enrollee.firstName} ${enrollee.lastName}</li>
							`;
					});
				});
			}
		});
	}
});

fetch(`https://mighty-lake-39348.herokuapp.com/api/courses/`)
.then(res => res.json())
.then(data => {
	// variable to store the card/message to show if there's no courses
	let courseData;
	if(data.length < 1) {
		courseData = "No courses available";
	}
	else {
		courseData = data.map(course => {
			if(adminUser === 'false' && course.isActive === false){
				return;
			}
			// logic for rendering different buttons based on the user
			if(adminUser === 'false' || !adminUser){
				cardFooter = 
					` 
						<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Select Course </a>
					`;
			}
			else{
				let toggleStatusBtn;
				if(course.isActive === true){
	                toggleStatusBtn = `<a href="./updateCourseStatus.html?courseId=${course._id}&status=false" value=${course._id} class="btn btn-danger text-white btn-block"> Disable Course </a>`;

				}
				else{
					toggleStatusBtn = `<a href="./updateCourseStatus.html?courseId=${course._id}&status=true" value=${course._id} class="btn btn-success text-white btn-block"> Enable Course </a>`;
				}
				adminViewBtn = `<button type="button" class="btn btn-xs btn-success float-right view-course-btn" data-courseId="${course._id}" data-toggle="modal" data-target="#viewCourseModal">
	                                	View
	                                </button>`;
				cardFooter = 
					`
						<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Edit </a>
						${toggleStatusBtn}
					`;

			}

			return `
						<div class="col-md-6 my-3">
	                        <div class='card'>
	                            <div class='card-body'>
	                                	${adminViewBtn}
	                                <h5 class='card-title'>
	                                	${course.name}
	                                </h5>
	                                <p class='card-text text-left'>
	                                    ${course.description}
	                                </p>
	                                <p class='card-text text-right'>
	                                   ₱ ${course.price}
	                                </p>

	                            </div>
	                            <div class='card-footer'>
	                                ${cardFooter}
	                            </div>
	                        </div>
	                    </div>
		            `;
		}).join("");

		let courseContainer = document.querySelector("#coursesContainer");
		courseContainer.innerHTML = courseData;

	}

});

let modalButton = document.querySelector("#adminButton");
if(adminUser === 'false' || !adminUser){
	modalButton.innerHTML = null;
}
else {
	modalButton.innerHTML = 
		`
			<div class="col-md-2 offset-md-10">
				<a href="./addCourse.html" class="btn btn-block btn-primary"> Add Course </a>
			</div>
		`;
}