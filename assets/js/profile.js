let profileContainer = document.querySelector("#profileContainer");
let token = localStorage.getItem('token');
let isAdmin = localStorage.getItem('isAdmin');
let userProfileForm = document.querySelector("#userProfile");

document.addEventListener('click', (e) => {
	let firstName = document.querySelector("#firstName");
	let lastName = document.querySelector("#lastName");
	if(e.target && e.target.id === "editProfileBtn"){
		fetch('https://mighty-lake-39348.herokuapp.com/api/users/details', {
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			firstName.value = data.firstName;
			lastName.value = data.lastName;
		});
	}
});

userProfileForm.addEventListener('submit', (e) => {
	e.preventDefault();
	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	fetch('https://mighty-lake-39348.herokuapp.com/api/users/updateProfile', {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${token}`
			},
			body: JSON.stringify({
				'firstName': firstName,
				'lastName': lastName
			})
		})
		.then(res => res.json())
		.then(data => {
			alert("Successfully updated profile");
			location.reload();
		});
})


// get user details to get enrolled items
fetch('https://mighty-lake-39348.herokuapp.com/api/users/details', {
	headers: {
		'Content-Type' : 'application/json',
		'Authorization' : `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	let formatDate = (date => {
		return ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear();
	})
	let enrolledCourses = data.enrollments.map(course => {
		return {
			courseId: course._id,
			date: formatDate(new Date(course.enrollled)),
			status: course.status
		}
	});
	// add the user details in the #profilecontainer
	let tableHTML = "";
	if(isAdmin === 'false'){
		tableHTML = 
			`
				<h3>Class History</h3>
				<table class="table">
					<thead>
						<tr>
							<th>Course</th>
							<th>Date</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody id='coursesEnrolled'>
					</tbody>
				</table>
			`
	}
	let userProfile = 
		`	
			<div class="col-md-12">
				<section class="jumbotron my-5">
					<a id="editProfileBtn" style="cursor: pointer;float: right;" data-toggle="modal" data-target="#editProfileModal"><i class="fas fa-edit"></i> Edit Profile</a>
					<div class="text-left mb-5">
						<h2>First Name: ${data.firstName}</h2>
						<h2>Last Name: ${data.lastName}</h2>
						<h2>Email: ${data.email}</h2>
					</div>	
					${tableHTML}
				</section>
			</div>
		`;
	profileContainer.innerHTML = userProfile;

	if(data.enrollments.length > 0) {
		let coursesEnrolledHTML = document.querySelector("#coursesEnrolled");
		let courseIds = data.enrollments.map(course => course.courseId);
		let promises = [];
		courseIds.forEach(courseId => {
		  promises.push(fetch(`https://mighty-lake-39348.herokuapp.com/api/courses/${courseId}`).then(res => res.json()).then(data => data));
		});
		Promise.all(promises)
		  .then(function handleData(data) {
			data.forEach((course, index) => {
				enrolledCourses[index].courseName = course.name;
				coursesEnrolledHTML.innerHTML += 
				`
					<tr>
						<td>${enrolledCourses[index].courseName}</td>
						<td>${enrolledCourses[index].date}</td>
						<td>${enrolledCourses[index].status}</td>
					</tr>
				`;
			});
		  })
		  .catch(function handleError(error) {
		    console.log("Error" + error);
		  });
	}
	else{
		document.querySelector("#coursesEnrolled").innerHTML = "<h2 class='text-center my-5'>You have no enrolled courses</h2>";
	}
	
});
