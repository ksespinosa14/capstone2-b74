// instantiate a URLSearchParams object so we can execute methods to access the parameters
let params = new URLSearchParams(window.location.search);

let courseId = params.get('courseId');
let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
let token = localStorage.getItem('token');
let currentUserId = localStorage.getItem('id');
//event for the dynamic button #enrollButton
document.addEventListener('click', (e) => {
	if(e.target && e.target.id== 'enrollButton'){
         fetch('https://mighty-lake-39348.herokuapp.com/api/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				alert("thank you for enrolling! See you in class!");
				window.location.replace("courses.html")
			}
			else{
				alert("something went wrong!");
			}
		});
     }
});

fetch(`https://mighty-lake-39348.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	let isEnrolled = data.enrollees.some(enrollee => enrollee.userId === currentUserId);
	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;
	if(!isEnrolled){
		enrollContainer.innerHTML = 
		`
			<button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>
		`;
	}
	else{
		enrollContainer.innerHTML = "<strong>You're Enrolled in this course</strong>";
	}
	
});